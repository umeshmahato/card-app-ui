import * as React from "react";
import Box from "@mui/material/Box";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import Accounts from "./views/Accounts";

const theme = createTheme();
export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Box>
        <main>
          <Router>
            <Routes>
              <Route
                exact
                path="/"
                element={<Navigate replace to="/accounts" />}
              />
              <Route exact path="/accounts" element={<Accounts />} />
            </Routes>
          </Router>
        </main>
      </Box>
    </ThemeProvider>
  );
}
