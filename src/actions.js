import axios from "axios";
import { SERVER_HOST } from "./constants";

export async function getAccountList() {
  try {
    let url = SERVER_HOST + `/v1/cards`;

    const result = await axios.get(url, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    // console.log("result", result.data);
    const { status, error, response = [] } = (result && result.data) || {};
    if (status === true) {
      return { response };
    } else {
      return { error };
    }
  } catch (e) {
    console.log("error", e.message);
    return { error: e.message };
  }
}

export async function createAccount({ request }) {
  try {
    const url = SERVER_HOST + "/v1/card";
    const result = await axios.post(url, request, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    // console.log("result", result.data);
    const { status, error, response = "" } = (result && result.data) || {};
    if (status === true) {
      return { response };
    } else {
      return { error };
    }
  } catch (e) {
    return { error: e.message };
  }
}
