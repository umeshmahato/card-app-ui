import {
  Alert,
  AppBar,
  Box,
  Button,
  Container,
  Grid,
  LinearProgress,
  Snackbar,
  Toolbar,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { getAccountList } from "../actions";
import AccountForm from "../components/AccountForm";
import DataGridTable from "../components/DataGridTable";

const Accounts = () => {
  const [loading, setLoading] = useState(false);
  const [results, setResults] = useState([]);

  const [toastOpen, setToastOpen] = useState(false);
  const [toastSeverity, setToastSeverity] = useState();
  const [toastMessage, setToastMessage] = useState();

  const [formOpen, setFormOpen] = useState(false);

  const handleFormClose = () => {
    setFormOpen(false);
  };

  const handleFormOpen = () => {
    setFormOpen(true);
  };

  const updateToast = (severity, message) => {
    setToastSeverity(severity);
    setToastMessage(message);
    setToastOpen(true);
  };

  const getAccounts = async () => {
    const { response, error } = await getAccountList();
    if (response) {
      setLoading(false);
      updateToast("success", "Accounts loaded!");
      setResults(response);
    } else updateToast("error", "Something went wrong!");
  };

  useEffect(() => {
    setLoading(true);
    getAccounts();
  }, []);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setToastOpen(false);
  };

  const handleRefreshAccountList = () => {
    getAccounts();
  };

  return (
    <>
      <AccountForm
        formOpen={formOpen}
        handleFormClose={handleFormClose}
        updateToast={updateToast}
        handleRefreshAccountList={handleRefreshAccountList}
      />
      <Snackbar
        open={toastOpen}
        autoHideDuration={2000}
        onClose={handleClose}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
      >
        <Alert
          onClose={handleClose}
          severity={toastSeverity}
          lg={{ width: "100%" }}
        >
          {toastMessage}
        </Alert>
      </Snackbar>
      <AppBar position="relative">
        <Toolbar>
          {/* <CameraIcon sx={{ mr: 2 }} /> */}
          <Grid container direction="row" spacing={4}>
            <Grid item xs={12} sm={6} md={10} lg={10}>
              <Typography variant="h6" color="inherit" noWrap>
                Credit card accounts
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={2} lg={2}>
              <Button
                size="small"
                variant="contained"
                color="success"
                onClick={handleFormOpen}
              >
                Add new
              </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Box
        sx={{
          bgcolor: "background.paper",
          pt: 5,
          pb: 5,
        }}
      >
        {loading && (
          <Container maxWidth="lg" sx={{ mt: 5 }}>
            <LinearProgress />
          </Container>
        )}

        {!loading && (
          <Container maxWidth="lg">
            <DataGridTable results={results} />
          </Container>
        )}
      </Box>
    </>
  );
};

export default Accounts;
