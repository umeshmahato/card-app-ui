import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import { format, parseISO } from "date-fns";

const columns = [
  { field: "name", headerName: "Name", width: 160 },
  { field: "card_number", headerName: "Card Number", width: 160 },
  { field: "limit", headerName: "Limit", width: 160 },
  { field: "balance", headerName: "Balance", width: 160 },
  {
    field: "created_at",
    headerName: "Created",
    sortable: false,
    width: 200,
    valueGetter: (params) =>
      format(parseISO(params.row.created_at), "dd MMM yyyy, hh:mm a"),
  },
];

export default function DataGridTable({ results }) {
  results.forEach((element) => {
    element.id = element._id;
  });
  return (
    <div style={{ height: 800, width: "100%" }}>
      <DataGrid
        rows={results}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        // checkboxSelection
      />
    </div>
  );
}
