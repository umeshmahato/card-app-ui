import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { createAccount } from "../actions";

const AccountForm = ({
  formOpen,
  handleFormClose,
  updateToast,
  handleRefreshAccountList,
}) => {
  const formInit = {
    name: "",
    card_number: "",
    limit: "",
  };
  const [formData, setFormData] = useState(formInit);
  const [errors, setErrors] = useState({});

  const validateForm = (formData) => {
    let errors = {};
    if (!formData.name) errors.name = "Name is required";
    if (!formData.card_number) errors.card_number = "Card number is required";
    if (formData.card_number && formData.card_number.length > 19)
      errors.card_number = "Card number max length should be 19";
    if (!formData.limit) errors.limit = "Limit is required";
    if (formData.limit && (formData.limit < 0 || formData.limit > 1000000))
      errors.limit = "Limit is allowed between 0 to 1000000";
    setErrors(errors);
    return Object.keys(errors).length == 0;
  };

  const handleFormSubmit = async () => {
    console.log({ formData });

    if (validateForm(formData)) {
      const { response, error } = await createAccount({ request: formData });
      if (error) updateToast("error", error);
      else {
        updateToast("success", "Account saved successfully!");
        handleRefreshAccountList();
        handleFormClose();
        setFormData(formInit);
      }
    }
  };
  return (
    <>
      <Dialog open={formOpen} onClose={handleFormClose}>
        <DialogTitle>Account Form</DialogTitle>
        <DialogContent>
          <TextField
            error={errors.name && errors.name != ""}
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            type="text"
            fullWidth
            variant="standard"
            value={formData.name}
            onChange={(event) =>
              setFormData({ ...formData, name: event.target.value })
            }
            helperText={errors.name}
          />
          <TextField
            error={errors.card_number && errors.card_number != ""}
            autoFocus
            margin="dense"
            id="card_number"
            label="Card Number"
            type="text"
            fullWidth
            variant="standard"
            value={formData.card_number}
            onChange={(event) =>
              setFormData({ ...formData, card_number: event.target.value })
            }
            helperText={errors.card_number}
          />
          <TextField
            InputProps={{ inputProps: { min: 0, max: 1000000 } }}
            error={errors.limit && errors.limit != ""}
            autoFocus
            margin="dense"
            id="limit"
            label="Limit"
            type="number"
            fullWidth
            variant="standard"
            value={formData.limit}
            onChange={(event) =>
              setFormData({ ...formData, limit: event.target.value })
            }
            helperText={errors.limit}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleFormClose}>Cancel</Button>
          <Button onClick={handleFormSubmit}>submit</Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AccountForm;
