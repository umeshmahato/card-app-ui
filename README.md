# Create React App using material for creating new Card accounts and Listing

UI Library used: MUI (https://mui.com/material-ui/getting-started/overview)

App Features:

1. Listing of credit card accounts.
2. Create new card account

Environment variables:

```sh
1. REACT_APP_API_HOST >> NodeJS serice base url.
```

Install it and run:

```sh
yarn
yarn start
```
